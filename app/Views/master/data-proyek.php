<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Data Proyek</h1>

    <a class="btn btn-info mb-3" href="">Tambah Data</a>

    <div class="row">
        <div class="col-lg">

            <table class="table text-center">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">No</th>
                        <th scope="col">Nama Proyek</th>
                        <th scope="col">Pricelist</th>
                        <th scope="col">Estimasi Komisi</th>
                        <th scope="col">Status Proyek</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="table-danger" scope="row">1</th>
                        <td>Square Garden Kalisuren</td>
                        <td>Rp390 JT</td>
                        <td>Rp2.5 JT</td>
                        <td>Tersedia</td>
                        <td><a class="btn btn-success" href="<?= base_url('dataMaster'); ?>/detailProyek">Detail</a></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>

</div>

<?= $this->endSection(); ?>