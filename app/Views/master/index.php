<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Data Member</h1>

    <a class="btn btn-info mb-3" href="/dataMaster/create">Tambah Data</a>

    <?php if (session()->getFlashdata('pesan')) : ?>
        <div class="alert alert-success" role="alert">
            <?= session()->getFlashdata('pesan'); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-7">
            <table class="table table-striped table-hover table-bordered table-sm text-center">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Member</th>
                        <th scope="col">Waktu Bergabung</th>
                        <th scope="col">Status Member</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($member as $m) : ?>
                        <tr>
                            <th scope="row">
                                <?= $i++; ?>
                            </th>
                            <td><?= $m['nama_member']; ?></td>
                            <td><?= $m['created_at']; ?></td>
                            <td><?= $m['status_member']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-3">
            <table class="table table-striped table-hover table-bordered table-sm text-center">
                <thead>
                    <tr>
                        <th scope="col">Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($user as $s) : ?>
                        <tr>
                            <td><?= $s['email']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="col-md-2">
            <table class="table table-borderless table-sm text-center">
                <thead>
                    <tr>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($member as $m) : ?>
                        <tr>
                            <td>
                                <a href="/dataMaster/delete/<?= $m['id_member']; ?>" class="badge btn-danger" onclick="return confirm('apakah anda yakin?');"><i class="fas fa-trash"></i></a>
                                <a href="/dataMaster/edit/<?= $m['id_member']; ?>" class="badge btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="/dataMaster/detail/<?= $m['id_member']; ?>" class="badge btn-success"><i class="fas fa-info-circle"></i></a>
                                <a href="/dataMaster/detail/<?= $m['id_member']; ?>" class="badge btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-info-circle"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    </div>


</div>
</div>
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row mt-3 justify-content-center">
                    <div class="col-md-4" style="text-align: left;">
                        <p>
                            <i class="fas fa-user"></i>&nbsp;
                            <span><?= $m['nama_member']; ?></span>
                        </p>
                        <p>
                            <i class="fas fa-map-marker-alt"></i>&nbsp;
                            <span><?= $m['alamat']; ?></span>
                        </p>
                        <p>
                            <i class="fas fa-envelope"></i>&nbsp;
                            <span><?= $s['email']; ?></span>
                        </p>
                        <p>
                            <i class="fas fa-phone"></i></i>&nbsp;
                            <span><?= $m['no_telepon']; ?></span>
                        </p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>