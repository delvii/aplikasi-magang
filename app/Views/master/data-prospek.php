<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Data & Status Prospek</h1>

    <div class="row">
        <div class="col-lg">

            <table class="table text-center">
                <thead>
                    <tr class="table-success">
                        <th scope="col">No</th>
                        <th scope="col">Nama Customer</th>
                        <th scope="col">Status Hubungan</th>
                        <th scope="col">No Telepon</th>
                        <th scope="col">Proyek Diminati</th>
                        <th scope="col">Jadwal Survei</th>
                        <th scope="col">Status Prospek</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="table-warning" scope="row">1</th>
                        <td>Gerry Julio</td>
                        <td>Keluarga</td>
                        <td>+0200300500</td>
                        <td>The Crystal Residence</td>
                        <td>30 Agustus 2021</td>
                        <td>Akad</td>
                        <td>
                            <a class="btn btn-info mb-2" href="">Ubah</a>
                            <a class="btn btn-success" href="<?= base_url('dataMaster'); ?>/detailProspek">Detail</a>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>

</div>

<?= $this->endSection(); ?>