<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Detail Proyek</h1>

    <a class="btn btn-info mb-3" href="">Ubah Data</a>

    <!-- Jumbotron -->
    <section class="jumbotron text-center">
        <div class="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <p>Foto</p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <p>Brosur</p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src="<?= base_url(); ?>/img/profile/default.jpg" class="img-thumbnail img-detail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="<?= base_url(); ?>/img/profile/default.jpg" class="img-thumbnail img-detail" />
            </div>
            <div class="col-md-1">
                <video controls>
                    <source src="" type="video/webm" />
                </video>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-6 text-left">
                <p>Nama Proyek :</p>
                <p>Pricelist :</p>
                <p>Estimasi Komisi :</p>
                <p>Status Proyek :</p>
                <p>Informasi Properti :</p>
                <p>Deskripsi :</p>
                <p>Fasilitas :</p>
                <p>Fasilitas Kesehatan :</p>
                <p>Fasilitas Pendidikan :</p>
                <p>Fasilitas Komersil :</p>
                <p>Wisata & Hiburan :</p>
                <p>Alamat :</p>
                <p>Kota :</p>
                <p>Developer :</p>
                <p>Lokasi :</p>
            </div>
            <div class="col-md-6 text-left">
                <p>Rujukan :</p>
                <p>Pimpinan Proyek :</p>
                <p>Jenis PKS :</p>
                <p>Target Perbulan :</p>
                <p>Siteplan :</p>
                <p>Sertifikat :</p>
                <p>PBB :</p>
                <p>Mou :</p>
                <p>Mou Start Date :</p>
                <p>Mou Expired Date :</p>
                <p>Cloudia :</p>
                <p>Persentase :</p>
                <p>Glob Komisi :</p>
                <p>Net Komisi :</p>
                <p>Non Zone :</p>
            </div>
        </div>
    </section>
    <!-- Akhir Jumbotron -->

</div>

<?= $this->endSection(); ?>