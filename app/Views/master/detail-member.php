<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Detail Member</h1>

    <!-- Jumbotron -->
    <section class="jumbotron">
        <div class="row">
            <div class="col" style="text-align: center;">
                <img src="/profile/<?= $member['foto_profile']; ?>" class="img-thumbnail img-detail" />
                <img src="/profile/<?= $member['foto_ktp']; ?>" class="img-thumbnail img-detail" />
                <img src="/profile/<?= $member['fotodiri_ktp']; ?>" class="img-thumbnail img-detail" />
                <img src="/profile/<?= $member['foto_rekening']; ?>" class="img-thumbnail img-detail" />
            </div>
        </div>
        <div class="row mt-3 justify-content-center">
            <div class="col-md-4" style="text-align: left;">
                <p>
                    <i class="fas fa-user"></i>&nbsp;
                    <span><?= $member['nama_member']; ?></span>
                </p>
                <p>
                    <i class="fas fa-map-marker-alt"></i>&nbsp;
                    <span><?= $member['alamat']; ?></span>
                </p>
                <p>
                    <i class="fas fa-envelope"></i>&nbsp;
                    <span><?= $user['email']; ?></span>
                </p>
                <p>
                    <i class="fas fa-phone"></i></i>&nbsp;
                    <span><?= $member['no_telepon']; ?></span>
                </p>
                <div class="row">
                    <div class="col">
                        <a href="/dataMaster" class="btn btn-info mt-3">Kembali</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="text-align: left;">
                <p>
                    <i class="fas fa-user"></i>&nbsp;
                    <span><?= $member['nama_rekening']; ?></span>
                </p>
                <p>
                    <i class="fas fa-map-marker-alt"></i>&nbsp;
                    <span><?= $member['nama_bank']; ?></span>
                </p>
                <p>
                    <i class="fas fa-hotel"></i></i>&nbsp;
                    <span><?= $member['no_rekening']; ?></span>
                </p>
            </div>
    </section>
    <!-- Akhir Jumbotron -->

</div>

<?= $this->endSection(); ?>