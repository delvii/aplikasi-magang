<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Profile</h1>

    <!-- Jumbotron -->
    <section class="jumbotron text-center">
        <div class="row">
            <h5>Ubah Profile</h5>
            <div class="col-md-5" style="text-align: left;">
                <img src="<?= base_url(); ?>/img/profile/default.jpg" class="rounded-circle img-thumbnail" /><br><br>
                <p>
                    <i class="fas fa-user"></i>&nbsp;
                    <span>John Doe</span>
                </p>
                <p>
                    <i class="fas fa-map-marker-alt"></i>&nbsp;
                    <span>San Francisco, CA</span>
                </p>
                <p>
                    <i class="fas fa-envelope"></i>&nbsp;
                    <span>john.doe@mail.com</span>
                </p>
                <p>
                    <i class="fas fa-phone"></i></i>&nbsp;
                    <span>+0200300500</span>
                </p>
            </div>
            <h5>Akun Bank</h5>
            <div class="col-md-4" style="text-align: left;">
                <img src="<?= base_url(); ?>/img/profile/default.jpg" class="rounded-circle img-thumbnail" /><br><br>
                <p>
                    <i class="fas fa-user"></i>&nbsp;
                    <span>Nama Rekening</span>
                </p>
                <p>
                    <i class="fas fa-map-marker-alt"></i>&nbsp;
                    <span>Nama Bank</span>
                </p>
                <p>
                    <i class="fas fa-hotel"></i></i>&nbsp;
                    <span>No Rekening</span>
                </p>
            </div>
        </div>
        <div class="row">
            <a class="btn btn-info mt-3" href="">Ubah Data</a>
        </div>
    </section>
    <!-- Akhir Jumbotron -->
</div>

<?= $this->endSection(); ?>