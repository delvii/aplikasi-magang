<?php

namespace App\Controllers;

use App\Models\MemberModel;
use App\Models\UserModel;


class DataMaster extends BaseController
{
    protected $memberModel;
    protected $userModel;
    public function __construct()
    {
        $this->memberModel = new MemberModel();
        $this->userModel = new UserModel();
    }

    public function index()
    {
        $data =
            [
                'title' => 'Data Member',
                'member' => $this->memberModel->getMember(),
                'user' => $this->userModel->getUser()
            ];

        return view('master/index', $data);
    }

    public function dataProyek()
    {
        $data =
            [
                'title' => 'Data Proyek'
            ];
        return view('master/data-proyek', $data);
    }

    public function dataProspek()
    {
        $data =
            [
                'title' => 'Data & Status Prospek'
            ];
        return view('master/data-prospek', $data);
    }

    public function detailProyek()
    {
        $data =
            [
                'title' => 'Detail Proyek'
            ];
        return view('master/detail-proyek', $data);
    }

    public function detailProspek()
    {
        $data =
            [
                'title' => 'Detail Prospek'
            ];
        return view('master/detail-prospek', $data);
    }

    public function create()
    {
        $data =
            [
                'title' => 'Form Tambah Data Member',
                'validation' => \Config\Services::validation()
            ];
        return view('master/tambah-member', $data);
    }

    public function save()
    {
        $method = $this->request->getVar();
        if ($method) {
            $rules = [
                'nama_member' =>
                [
                    'rules' => 'required|is_unique[member_profile.nama_member]',
                    'errors' =>
                    [
                        'required' => 'Nama Member harus diisi',
                        'is_unique' => 'Nama Member sudah ada di database'
                    ]
                ],
                'alamat' =>
                [
                    'rules' => 'required',
                    'errors' =>
                    [
                        'required' => 'Alamat harus diisi'
                    ]
                ],
                'no_telepon' =>
                [
                    'rules' => 'required',
                    'errors' =>
                    [
                        'required' => 'No Telepon harus diisi'
                    ]
                ],
                'nama_rekening' =>
                [
                    'rules' => 'required',
                    'errors' =>
                    [
                        'required' => 'Nama Rekening harus diisi'
                    ]
                ],
                'nama_bank' =>
                [
                    'rules' => 'required',
                    'errors' =>
                    [
                        'required' => 'Nama Bank harus diisi'
                    ]
                ],
                'no_rekening' =>
                [
                    'rules' => 'required',
                    'errors' =>
                    [
                        'required' => 'No Rekening harus diisi'
                    ]
                ],
                'foto_ktp' =>
                [
                    'rules' => 'max_size[foto_ktp,1024]|is_image[foto_ktp]|mime_in[foto_ktp, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'fotodiri_ktp' =>
                [
                    'rules' => 'max_size[fotodiri_ktp,1024]|is_image[fotodiri_ktp]|mime_in[fotodiri_ktp, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'foto_profile' =>
                [
                    'rules' => 'max_size[foto_profile,1024]|is_image[foto_profile]|mime_in[foto_profile, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'foto_rekening' =>
                [
                    'rules' => 'max_size[foto_rekening,1024]|is_image[foto_rekening]|mime_in[foto_rekening, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'email' => [
                    'rules' => 'required|valid_email|is_unique[user.email]',
                    'errors' =>
                    [
                        'required' => 'Email harus diisi',
                        'is_unique' => 'Email sudah ada di database'
                    ]
                ],
                'password' => [
                    'rules' => 'required|min_length[6]',
                    'errors' =>
                    [
                        'required' => 'Password harus diisi'
                    ]
                ],
                'cpassword' => [
                    'rules' => 'required|matches[password]',
                    'errors' =>
                    [
                        'required' => 'Konfirmasi Password harus diisi'
                    ]
                ]
            ];

            if ($this->validate($rules)) {
                //ambil gambar
                $foto_ktp = $this->request->getFile('foto_ktp');
                //apakah tidak ada gambar yang diupload
                if ($foto_ktp->getError() == 4) {
                    $Foto_Ktp = 'default.jpg';
                } else {
                    //generate nama gambar random
                    $Foto_Ktp = $foto_ktp->getRandomName();
                    //pindahkan file ke folder img
                    $foto_ktp->move('profile', $Foto_Ktp);
                }
                //ambil gambar
                $fotodiri_ktp = $this->request->getFile('fotodiri_ktp');
                //apakah tidak ada gambar yang diupload
                if ($fotodiri_ktp->getError() == 4) {
                    $FotoDiri_Ktp = 'default.jpg';
                } else {
                    //generate nama gambar random
                    $FotoDiri_Ktp = $fotodiri_ktp->getRandomName();
                    //pindahkan file ke folder img
                    $fotodiri_ktp->move('profile', $FotoDiri_Ktp);
                }
                //ambil gambar
                $foto_profile = $this->request->getFile('foto_profile');
                //apakah tidak ada gambar yang diupload
                if ($foto_profile->getError() == 4) {
                    $Foto_Profile = 'default.jpg';
                } else {
                    //generate nama gambar random
                    $Foto_Profile = $foto_profile->getRandomName();
                    //pindahkan file ke folder img
                    $foto_profile->move('profile', $Foto_Profile);
                }
                //ambil gambar
                $foto_rekening = $this->request->getFile('foto_rekening');
                //apakah tidak ada gambar yang diupload
                if ($foto_rekening->getError() == 4) {
                    $Foto_Rekening = 'default.jpg';
                } else {
                    //generate nama gambar random
                    $Foto_Rekening = $foto_rekening->getRandomName();
                    //pindahkan file ke folder img
                    $foto_rekening->move('profile', $Foto_Rekening);
                }
                $status = $this->request->getVar('status_member');
                if ($status === null) {
                    $status_default = 'aktif';
                }
                $newUser = [
                    'nama_member' => $this->request->getVar('nama_member'),
                    'alamat' => $this->request->getVar('alamat'),
                    'no_telepon' => $this->request->getVar('no_telepon'),
                    'nama_rekening' => $this->request->getVar('nama_rekening'),
                    'nama_bank' => $this->request->getVar('nama_bank'),
                    'no_rekening' => $this->request->getVar('no_rekening'),
                    'keterangan' => $this->request->getVar('keterangan'),
                    'status_member' => $status_default,
                    'foto_ktp' => $Foto_Ktp,
                    'fotodiri_ktp' => $FotoDiri_Ktp,
                    'foto_profile' => $Foto_Profile,
                    'foto_rekening' => $Foto_Rekening
                ];

                $role = $this->request->getVar('role');
                if ($role === null) {
                    $role_default = 'member';
                }
                $user = [
                    'email' => $this->request->getVar('email'),
                    'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                    'role' => $role_default
                ];

                $this->memberModel->save($newUser);
                $this->userModel->save($user);

                session()->setFlashdata('pesan', 'Data berhasil ditambahkan');
                return redirect()->to('/dataMaster');
            }
        }
        return redirect()->to('/dataMaster/create')->withInput();
    }

    public function delete($id_member)
    {
        $member = $this->memberModel->find($id_member);
        $this->userModel->find($id_member);
        //cek jika file gambarnya default.jpg
        if ($member['foto_ktp'] != 'default.jpg') {
            //hapus gambar
            unlink('profile/' . $member['foto_ktp']);
        }
        //cek jika file gambarnya default.jpg
        if ($member['fotodiri_ktp'] != 'default.jpg') {
            //hapus gambar
            unlink('profile/' . $member['fotodiri_ktp']);
        }
        //cek jika file gambarnya default.jpg
        if ($member['foto_profile'] != 'default.jpg') {
            //hapus gambar
            unlink('profile/' . $member['foto_profile']);
        }
        //cek jika file gambarnya default.jpg
        if ($member['foto_rekening'] != 'default.jpg') {
            //hapus gambar
            unlink('profile/' . $member['foto_rekening']);
        }
        //hapus data
        $this->memberModel->delete($id_member);
        $this->userModel->delete($id_member);
        session()->setFlashdata('pesan', 'Data berhasil dihapus');
        return redirect('master/index');
    }

    public function detail($id_member)
    {
        $data =
            [
                'title' => 'Detail Member',
                'member' => $this->memberModel->getMember($id_member),
                'user' => $this->userModel->getUser($id_member)
            ];

        if (empty($data['member'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('ID ' . $id_member . ' tidak ditemukan');
        }

        return view('master/detail-member', $data);
    }


    public function edit($id_member)
    {
        $data =
            [
                'title' => 'Form Ubah Data Member',
                'validation' => \Config\Services::validation(),
                'member' => $this->memberModel->getMember($id_member),
                'user' => $this->userModel->getUser($id_member)
            ];
        return view('master/edit-member', $data);
    }

    public function update($id_member)
    {
        $this->memberModel->getMember($this->request->getVar('id_member'));
        $this->userModel->getUser($this->request->getVar('id_member'));

        $method = $this->request->getVar();
        if ($method) {
            $rules = [
                'nama_member' =>
                [
                    'rules' => 'required',
                    'errors' =>
                    [
                        'required' => 'Nama Member harus diisi'
                    ]
                ],
                'foto_ktp' =>
                [
                    'rules' => 'max_size[foto_ktp,1024]|is_image[foto_ktp]|mime_in[foto_ktp, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'fotodiri_ktp' =>
                [
                    'rules' => 'max_size[fotodiri_ktp,1024]|is_image[fotodiri_ktp]|mime_in[fotodiri_ktp, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'foto_profile' =>
                [
                    'rules' => 'max_size[foto_profile,1024]|is_image[foto_profile]|mime_in[foto_profile, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'foto_rekening' =>
                [
                    'rules' => 'max_size[foto_rekening,1024]|is_image[foto_rekening]|mime_in[foto_rekening, image/jpg,image/jpeg,image/png]',
                    'errors' =>
                    [
                        'max_size' => 'Ukuran gambar terlalu besar',
                        'is_image' => 'Yang dipilih bukan gambar',
                        'mime_in' => 'Yang dipilih bukan gambar'
                    ]
                ],
                'email' => [
                    'rules' => 'required|valid_email',
                    'errors' =>
                    [
                        'required' => 'Email harus diisi'
                    ]
                ],
                'password' => [
                    'rules' => 'required|min_length[6]',
                    'errors' =>
                    [
                        'required' => 'Password harus diisi'
                    ]
                ],
                'cpassword' => [
                    'rules' => 'required|matches[password]',
                    'errors' =>
                    [
                        'required' => 'Konfirmasi Password harus diisi'
                    ]
                ]
            ];

            if ($this->validate($rules)) {
                $foto_ktp = $this->request->getFile('foto_ktp');
                //cek gambar, apakah tetap gambar lama
                if ($foto_ktp->getError() == 4) {
                    $Foto_Ktp = $this->request->getVar('foto_ktpLama');
                } else {
                    //generate nama file random
                    $Foto_Ktp = $foto_ktp->getRandomName();
                    //pindahkan gambar
                    $foto_ktp->move('profile', $Foto_Ktp);
                    //hapus file lama
                    unlink('profile/' . $this->request->getVar('foto_ktpLama'));
                }
                $fotodiri_ktp = $this->request->getFile('fotodiri_ktp');
                //cek gambar, apakah tetap gambar lama
                if ($fotodiri_ktp->getError() == 4) {
                    $FotoDiri_Ktp = $this->request->getVar('fotodiri_ktpLama');
                } else {
                    //generate nama file random
                    $FotoDiri_Ktp = $fotodiri_ktp->getRandomName();
                    //pindahkan gambar
                    $fotodiri_ktp->move('profile', $FotoDiri_Ktp);
                    //hapus file lama
                    unlink('profile/' . $this->request->getVar('fotodiri_ktpLama'));
                }
                $foto_profile = $this->request->getFile('foto_profile');
                //cek gambar, apakah tetap gambar lama
                if ($foto_profile->getError() == 4) {
                    $Foto_Profile = $this->request->getVar('foto_profileLama');
                } else {
                    //generate nama file random
                    $Foto_Profile = $foto_profile->getRandomName();
                    //pindahkan gambar
                    $foto_profile->move('profile', $Foto_Profile);
                    //hapus file lama
                    unlink('profile/' . $this->request->getVar('foto_profileLama'));
                }
                $foto_rekening = $this->request->getFile('foto_rekening');
                //cek gambar, apakah tetap gambar lama
                if ($foto_rekening->getError() == 4) {
                    $Foto_Rekening = $this->request->getVar('foto_rekeningLama');
                } else {
                    //generate nama file random
                    $Foto_Rekening = $foto_rekening->getRandomName();
                    //pindahkan gambar
                    $foto_rekening->move('profile', $Foto_Rekening);
                    //hapus file lama
                    unlink('profile/' . $this->request->getVar('foto_rekeningLama'));
                }
                $newUser = [
                    'id_member' => $id_member,
                    'nama_member' => $this->request->getVar('nama_member'),
                    'alamat' => $this->request->getVar('alamat'),
                    'no_telepon' => $this->request->getVar('no_telepon'),
                    'nama_rekening' => $this->request->getVar('nama_rekening'),
                    'nama_bank' => $this->request->getVar('nama_bank'),
                    'no_rekening' => $this->request->getVar('no_rekening'),
                    'keterangan' => $this->request->getVar('keterangan'),
                    'foto_ktp' => $Foto_Ktp,
                    'fotodiri_ktp' => $FotoDiri_Ktp,
                    'foto_profile' => $Foto_Profile,
                    'foto_rekening' => $Foto_Rekening
                ];

                $role = $this->request->getVar('role');
                if ($role === null) {
                    $role_default = 'member';
                }
                $user = [
                    'id_member' => $this->request->getVar('id_member'),
                    'email' => $this->request->getVar('email'),
                    'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                    'role' => $role_default
                ];

                $this->memberModel->save($newUser);
                $this->userModel->save($user);

                session()->setFlashdata('pesan', 'Data berhasil ubah');
                return redirect()->to('/dataMaster');
            }
        }
        return redirect()->to('/dataMaster/edit/' . $this->request->getVar('id_member'))->withInput();
    }
}
