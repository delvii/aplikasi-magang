<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data =
			[
				'title' => 'Halaman Dashboard',
			];

		return view('dashboard/index', $data);
	}

	public function pesan()
	{
		$data =
			[
				'title' => 'Halaman Pesan'
			];
		return view('dashboard/pesan', $data);
	}

	public function profile()
	{
		$data =
			[
				'title' => 'Halaman Profile'
			];
		return view('dashboard/profile', $data);
	}
}
