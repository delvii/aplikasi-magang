<?php

namespace App\Controllers;

class Maintenance extends BaseController
{
    public function index()
    {
        $data =
            [
                'title' => 'User & Role',
            ];

        return view('maintenance/index', $data);
    }

    public function maintenance()
    {
        $data =
            [
                'title' => 'Backup, Restore, Import Data'
            ];
        return view('maintenance/maintenance', $data);
    }
}
