<?php

namespace App\Controllers;

class Laporan extends BaseController
{
    public function index()
    {
        $data =
            [
                'title' => 'Laporan Prospek',
            ];

        return view('laporan/index', $data);
    }

    public function pengguna()
    {
        $data =
            [
                'title' => 'Laporan Pengguna'
            ];
        return view('laporan/laporan-pengguna', $data);
    }

    public function komisi()
    {
        $data =
            [
                'title' => 'Laporan Komisi'
            ];
        return view('laporan/laporan-komisi', $data);
    }
}
