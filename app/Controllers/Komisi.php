<?php

namespace App\Controllers;

class Komisi extends BaseController
{
    public function index()
    {
        $data =
            [
                'title' => 'Komisi Admin',
            ];

        return view('komisi/index', $data);
    }

    public function komisiUser()
    {
        $data =
            [
                'title' => 'Komisi User'
            ];
        return view('komisi/komisi-user', $data);
    }
}
